from django.urls import path, include

from mav.views import (CommandCreateAPIView,)


urlpatterns = [
    path('command/', CommandCreateAPIView.as_view()),
]