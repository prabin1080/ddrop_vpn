#!/usr/bin/env python3

import asyncio
from mavsdk import System

from mavsdk.generated.action import ActionError


async def arm():

    drone = System()
    await drone.connect(system_address="udp://:14550")

    print("Waiting for drone...")
    async for state in drone.core.connection_state():
        if state.is_connected:
            print(f"Drone discovered with UUID: {state.uuid}")
            break


    print("-- Arming")
    await drone.action.arm()

async def disarm():

    drone = System()
    await drone.connect(system_address="udp://:14550")

    print("Waiting for drone...")
    async for state in drone.core.connection_state():
        if state.is_connected:
            print(f"Drone discovered with UUID: {state.uuid}")
            break


    print("-- Killing")
    await drone.action.kill()

    # await asyncio.sleep(2)

    # while True:
    #     try:
    #         await drone.action.disarm()
    #         break
    #     except ActionError:
    #         print('ActionError')


    # print("-- Taking off")
    # await drone.action.takeoff()

    # await asyncio.sleep(2)

    # print("-- Landing")
    # await drone.action.land()

    # await asyncio.sleep(2)

    # await drone.action.disarm()


# async def print_flight_mode():

#     async for flight_mode in drone.telemetry.flight_mode():
#         print("FlightMode:", flight_mode)


def run_arm(command=None):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
    # loop.run_until_complete(print_flight_mode())
