import asyncio
from django.conf import settings

from rest_framework import status
from rest_framework.generics import (CreateAPIView, ListAPIView, RetrieveUpdateAPIView,)
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from .serializers import CommandSerializer

from .mav_commands import arm, disarm

loop = asyncio.get_event_loop()



class CommandCreateAPIView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CommandSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.data['command'] == 'arm':
            loop.run_until_complete(arm())
        elif serializer.data['command'] == 'disarm':
            loop.run_until_complete(disarm())
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)