from rest_framework import serializers


choices = (
    ('arm', 'arm'),
    ('disarm', 'disarm'),
)


class CommandSerializer(serializers.Serializer):
    command = serializers.ChoiceField(choices)